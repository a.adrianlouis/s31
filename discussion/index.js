// Http module is what will allow javascript to create/start a aserver
const http = require("http");

const port = 4000

// Http module has access to the 'createServer' function. The 'createServer' function is responsible for creating a server.
// The request object, contains data coming from outside of the server (ex. forms)
// The respose objects, contains data coming from the server itself
http.createServer(function(request, response) {

	// Using the response object, the 'writeHead' function writes the header for this response
	// The first argument for 'writeHead' is the HTTP status code, the second argument is the type  of content that the response will hold
	response.writeHead(200, {'Content-Type': 'text/plain'})

	// The last part is using the response object and accessing 'end' function which will end the response and attach data to it
	// Note: The data inside the response must match the content type that is set in the header
	response.end('Hello World!')
}).listen(port) // The 'listen' function sets the part where this server will run

console.log('Server is running at localhost:4000')